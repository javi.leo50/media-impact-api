<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    use HasFactory;

    protected $table = "permissions";

    public function rolRel()
    {
        return $this->belongsTo('App\Models\Role', 'rol_id', 'id');
    }

    public function module()
    {
        return $this->belongsTo('App\Models\MasterModules');
    }
}
