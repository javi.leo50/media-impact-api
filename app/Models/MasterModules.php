<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterModules extends Model
{
    use HasFactory;

    protected $table = 'master_modules';
    protected $guarded = [];
}
