<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

use Carbon\Carbon;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    protected $appends = ['avatar_initials', "created_at_format"];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function rolRel()
    {
        return $this->belongsTo('App\Models\Role', 'rol_id', 'id');
    }

    public function getCreatedAtFormatAttribute()
    {
        $date = Carbon::parse($this->created_at);
        return $date->isoFormat('LLLL');
    }

    public function getAvatarInitialsAttribute()
    {
        $full_name = $this->name . " " . $this->last_name;
        $temp = explode(' ', trim($full_name));
        $initials = strtoupper(substr($temp[0], 0, 1));
        if (count($temp) > 1) {
            $initials = $initials . strtoupper(substr($temp[1], 0, 1));
        }
        return $initials;
    }
}
