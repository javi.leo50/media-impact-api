<?php

namespace App\Models;

use App\Transformers\API\v1\Customer\CustomerTraformer;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;

    public $transformer = CustomerTraformer::class;

    protected $table = 'customers';

    protected $appends = ['avatar_initials', "created_at_format", "age", "gender_format"];

    public function getCreatedAtFormatAttribute()
    {
        $date = Carbon::parse($this->created_at);
        return $date->isoFormat('LLLL');
    }

    public function getGenderFormatAttribute()
    {
        $return = "";

        if ($this->gender == 0) {
            $return = "Masculino";
        } else if ($this->gender == 1) {
            $return = "Femenino";
        } else {
            $return = "No definido";
        }
        return $return;
    }

    public function getAvatarInitialsAttribute()
    {
        $full_name = $this->name . " " . $this->last_name;
        $temp = explode(' ', trim($full_name));
        $initials = strtoupper(substr($temp[0], 0, 1));
        if (count($temp) > 1) {
            $initials = $initials . strtoupper(substr($temp[1], 0, 1));
        }
        return $initials;
    }

    public function getAgeAttribute()
    {
        $date_birthday = Carbon::parse($this->date_birthday);
        if ($date_birthday <= Carbon::now()) {
            $anios = Carbon::now()->diffInYears($date_birthday);
        } else {
            $anios = 0;
        }
        return (int) $anios;
    }
}
