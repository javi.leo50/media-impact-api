<?php

namespace App\UseCases\API\v1\Helpers\Storage;

class DownloadFileUseCase
{
    protected $storage;

    public function __construct($storage)
    {
        $this->storage = $storage;
    }

    public function execute($path)
    {
        if ($this->storage->exists($path)) {
            return response()->download(storage_path('app/private' . $path), null, [], null);
        } else {
            return abort(404);
        }
    }
}
