<?php

namespace App\UseCases\API\v1\Helpers\Storage;

class GenerateFileUseCase
{
    protected $storage;

    public function __construct($storage)
    {
        $this->storage = $storage;
    }

    public function execute($file, $folder, $initial)
    {
        $name_file = $this->setFileName("$initial-" . time(), $file);
        $this->storage->putFileAs($folder, $file, $name_file);
        return $name_file;
    }

    public function setFileName($name, $file)
    {
        $nameFile = $name . time() . uniqid() . '.' . $file->getClientOriginalExtension();
        return $nameFile;
    }
}
