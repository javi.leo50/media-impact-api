<?php

namespace App\UseCases\API\v1\Customer;

use App\DTOs\API\v1\Customer\CreateCustomerDto;
use App\DTOs\API\v1\Helpers\Storage\GenerateFileDto;
use App\Models\Customer;
use App\UseCases\API\v1\Helpers\Storage\GenerateFileUseCase;
use Carbon\Carbon;

class CreateCustomerUseCase
{
    private const FOLDER_IMAGE = 'images' . DIRECTORY_SEPARATOR . 'customers';
    private const INITIAL_IMAGE = 'photo';

    protected $customer;
    protected $storage;

    public function __construct(Customer $customer, $storage)
    {
        $this->customer = $customer;
        $this->storage = $storage;
    }

    public function execute(CreateCustomerDto $customerDto)
    {
        $fileImage = "";

        if ($customerDto->file_image) {
            $fileImageDto = GenerateFileDto::fromArray(['file' => $customerDto->file_image]);
            $generate = new GenerateFileUseCase($this->storage);
            $fileImage = $generate->execute($fileImageDto->file, self::FOLDER_IMAGE, self::INITIAL_IMAGE);
        }

        $this->customer->date_birthday = (new Carbon($customerDto->date_birthday))->format('Y-m-d');
        $this->customer->name = $customerDto->name;
        $this->customer->last_name = $customerDto->last_name;
        $this->customer->email = $customerDto->email;
        $this->customer->gender = $customerDto->gender;
        $this->customer->file_image = $fileImage;

        $this->customer->save();

        return true;
    }
}
