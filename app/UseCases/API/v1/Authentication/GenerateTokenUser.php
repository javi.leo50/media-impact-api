<?php

namespace App\UseCases\API\v1\Authentication;

use App\DTOs\API\v1\Authentication\LoginDto;
use App\Models\User;
use Carbon\Carbon;

class GenerateTokenUser
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function execute(LoginDto $loginDto)
    {
        $user = $this->user;
        $tokenUser = $user->createToken('Personal Access Token');
        $token = $tokenUser->token;
        if ($loginDto->remember_me) {
            $token->expires_at = Carbon::now()->addMonths(1);
        }
        $token->save();

        return [
            'tokenUser' => $tokenUser,
            'user' => $user
        ];
    }
}
