<?php

namespace App\UseCases\API\v1\Authentication;

use App\DTOs\API\v1\Authentication\LoginDto;

use Illuminate\Support\Facades\Auth;

class VerifyUser
{
    public function execute(LoginDto $loginDto)
    {
        $credentials = [
            'email' => $loginDto->email,
            'password' => $loginDto->password
        ];
        if (!Auth::attempt($credentials)) {
            return false;
        }

        return Auth::user();
    }
}
