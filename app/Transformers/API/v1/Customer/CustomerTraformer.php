<?php

namespace App\Transformers\API\v1\Customer;

use App\Models\Customer;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class CustomerTraformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Customer $customer)
    {
        $array =  [
            "id" => $customer->id,
            "name" => $customer->name,
            "last_name" => $customer->last_name,
            "email" => $customer->email,
            "file_image" => $customer->file_image,
            "gender" => (int) $customer->gender,
            "gender_format" => $customer->gender_format,
            "avatar_initials" => $customer->avatar_initials,
            "date_birthday" => (new Carbon($customer->date_birthday))->format('d-m-Y'),
            "age" => $customer->age,
            "created_at_format" => $customer->created_at_format,
        ];

        return $array;
    }

    public static function originalAttribute($index)
    {
        $attributes = [
            "id" => 'id',
            "name" => 'name',
            "last_name" => 'last_name',
            "email" => 'email',
            "date_birthday" => "date_birthday",
            "file_image" => 'file_image',
            "created_at"  => 'created_at',

            '_method' => '_method',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            "id" => 'id',
            "name" => 'name',
            "last_name" => 'last_name',
            "email" => 'email',
            "date_birthday" => "date_birthday",
            "file_image" => 'file_image',
            "created_at"  => 'created_at',

            '_method' => '_method',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}
