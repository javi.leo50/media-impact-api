<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Model as Model;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Serializer\ArraySerializer;
use Spatie\Fractal\Facades\Fractal;

trait ApiResponser
{
    private function successResponse($data, $code)
    {
        return response()->json($data, $code);
    }

    protected function errorResponse($message, $code)
    {
        return response()->json(['error' => $message, 'code' => $code], $code);
    }

    protected function showData($data, $code = 200)
    {
        return $this->successResponse(['data' => $data], $code);
    }

    protected function transformData($data, $transformer)
    {
        $transformation = Fractal::create($data, new $transformer);
        return $transformation->toArray();
    }

    protected function showOne(Model $instance, $code = 200)
    {
        $transformer = $instance->transformer;
        $instance = $this->transformData($instance, $transformer);

        return $this->successResponse($instance, $code);
    }

    protected function showAll($collection, $code = 200)
    {
        if (!$collection->first()) {
            return $this->successResponse(['data' => []], $code);
        }

        $transformer = $collection->first()->transformer;
        if (request()->has('sort_by')) {
            $attribute = $transformer::originalAttribute(request()->sort_by);
            if ($attribute) {
                $collection = $collection->orderBy($attribute, request('descending'))->paginate((int)request('per_page'));
            } else {
                $collection = $collection->paginate((int)request('per_page'));
            }
        } else {
            $collection = $collection->paginate((int)request('per_page'));
        }

        $get_collection = $collection->getCollection();

        $collection = Fractal::create()
            ->collection($get_collection, new $transformer)
            ->serializeWith(new ArraySerializer())
            ->paginateWith(new IlluminatePaginatorAdapter($collection))
            ->toArray();

        return $this->successResponse($collection, $code);
    }
}
