<?php

namespace App\Http\Controllers\API\v1\Customer;

use App\DTOs\API\v1\Customer\CreateCustomerDto;
use App\DTOs\API\v1\Customer\UpdateCustomerDto;
use App\Http\Controllers\API\v1\ApiController;
use App\Http\Requests\API\v1\Customer\CustomerRequest;
use App\Models\Customer;
use App\UseCases\API\v1\Customer\CreateCustomerUseCase;
use App\UseCases\API\v1\Customer\UpdateCustomerUseCase;
use Illuminate\Support\Facades\Storage;

use Illuminate\Http\Request;

class CustomerController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $q = $request->q;
        $customers = new Customer();
        if ($q) {
            $customers = $customers->where(function ($query) use ($q) {
                $query->where('name', 'LIKE', '%' . $q . '%');
                $query->orWhere('last_name', 'LIKE', '%' . $q . '%');
                $query->orWhere('email', 'LIKE', '%' . $q . '%');
                $query->orWhere('date_birthday', 'LIKE', '%' . $q . '%');
            });
        }
        return $this->showAll($customers);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CustomerRequest $request)
    {
        $customerDto = CreateCustomerDto::fromRequest($request);
        $element = new CreateCustomerUseCase(new Customer(), Storage::disk('s3'));
        $element->execute($customerDto);
        return $this->showData([
            'title' => trans('custom.title.success'),
            'message' => trans('custom.message.create.success', ['name' => trans('custom.attribute.customer')])
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        return $this->showOne($customer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(CustomerRequest $request, Customer $customer)
    {
        $customerDto = UpdateCustomerDto::fromRequest($request);
        if (!$request->hasFile('file_image')) {
            $customerDto->file_image = null;
        }
        $element = new UpdateCustomerUseCase($customer, Storage::disk('s3'));
        $element->execute($customerDto);
        return $this->showData([
            'title' => trans('custom.title.success'),
            'message' => trans('custom.message.update.success', ['name' => trans('custom.attribute.customer')])
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        $image = $customer->file_image;
        $customer_delete = $customer->delete();
        if ($customer_delete) {
            Storage::disk('s3')->delete('images/customers/' . $image);
        }
        return $this->showData([
            'title' => trans('custom.title.success'),
            'message' => trans('custom.message.delete.success', ['name' => trans('custom.attribute.customer')])
        ]);
    }
}
