<?php

namespace App\Http\Controllers\API\v1\User;

use App\Http\Controllers\API\v1\ApiController;
use App\Models\MasterModules;

class UserController extends ApiController
{
    public function user()
    {
        $user = auth()->user();
        $array_modules = array();
        $modules = MasterModules::select('id', 'nameref', 'name', 'slug', 'parent', 'icon', 'index')->get();
        foreach ($modules as $key => $value) {
            $data = [
                'id' => $value->id,
                'name' => $value->name,
                'slug' => $value->slug,
                'nameref' => $value->nameref,
                'parent' => $value->parent,
                'icon' => $value->icon,
                'index' => $value->index,
                'q_subestados' => array(
                    'listar', 'registrar', 'editar'
                )
            ];
            array_push($array_modules, $data);
        }
        $user->modules = $array_modules;

        return $this->showData($user);
    }
}
