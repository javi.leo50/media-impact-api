<?php

namespace App\Http\Controllers\API\v1\Dashboard;

use App\Http\Controllers\API\v1\ApiController;
use App\Http\Requests\API\v1\Dashboard\FilterDataDashboardRequest;
use App\Models\Customer;
use Carbon\Carbon;

class DashboardController extends ApiController
{
    public function index()
    {
        $end_range_date_card = Carbon::now();
        $start_range_date_card = Carbon::now()->subMonth()->subDay();

        //range dates card
        $data_range_ = array(
            'end' => $end_range_date_card->format('d-m-Y'),
            'start' =>  $start_range_date_card->format('d-m-Y'),
        );

        /************************************************************************************** */
        $start_date_filter = Carbon::now()->subMonth()->subDay();
        $end_date_filter = Carbon::now();

        //Inicio Data Cards
        $data_card = $this->getDataCard($start_date_filter, $end_date_filter);
        //Fin Data Card

        /***************************************************************************************** */

        //Inicio data pies  
        ////porcentaje de usuario pos generos registrados de acuerdo a la fecha ingresada
        $data_pies_1 = $this->getGraphPie1($start_date_filter, $end_date_filter);
        //// fin 

        ////edades de los usuarios ed acuerdo a la fecha ingresada
        $data_pies_2 = $this->getGraphPie2($start_date_filter, $end_date_filter);
        ////fin 

        //Fin data pies

        /********************************************************************************************** */

        //COLUMN
        $data_column_1 = $this->getGraphColumn1();
        //FIN COLUMN

        /************************************************************************************************ */

        $data_retornar = [
            //dates
            'data_range' => $data_range_,
            //datas
            'data_card' => $data_card,
            'data_pies_1' => $data_pies_1,
            'data_pies_2' => $data_pies_2,

            'data_column_1' => $data_column_1,
        ];
        return $this->showData($data_retornar);
    }

    public function getDataFilterCards(FilterDataDashboardRequest $request)
    {
        $end = Carbon::createFromFormat('d-m-Y', $request->end);
        $start = Carbon::createFromFormat('d-m-Y', $request->start)->subDay();

        $data_card = $this->getDataCard($start, $end);

        $data_retornar = [
            'data_card' => $data_card,
        ];
        return $this->showData($data_retornar);
    }

    public function getDataFilterPies(FilterDataDashboardRequest $request)
    {
        $end_date_filter = Carbon::createFromFormat('d-m-Y', $request->end);
        $start_date_filter = Carbon::createFromFormat('d-m-Y', $request->start)->subDay();

        //Inicio data pies  
        ////porcentaje de usuario pos generos registrados de acuerdo a la fecha ingresada
        $data_pies_1 = $this->getGraphPie1($start_date_filter, $end_date_filter);
        //// fin 

        ////edades de los usuarios ed acuerdo a la fecha ingresada
        $data_pies_2 = $this->getGraphPie2($start_date_filter, $end_date_filter);
        ////fin 

        //Fin data pies

        /********************************************************************************************** */

        $data_retornar = [
            'data_pies_1' => $data_pies_1,
            'data_pies_2' => $data_pies_2,
        ];
        return $this->showData($data_retornar);
    }

    public function getDataCard($start_date, $end_date)
    {
        $customers = Customer::whereBetween('created_at', [$start_date, $end_date])->get();

        $sum_ages = 0;
        foreach ($customers as $key => $value) {
            $sum_ages = $sum_ages + $value->age;
        }
        if ($sum_ages == 0) {
            $average = 0;
        } else {
            $average = $sum_ages / COUNT($customers);
        }

        $max_age = Customer::whereBetween('created_at', [$start_date, $end_date])->orderBy('date_birthday', 'ASC')->first();
        if (!$max_age) {
            $max_age = 0;
        } else {
            $max_age = $max_age->age . " años";
        }

        //desviación estandar
        $standard_deviation = 0;
        $sum_of_squares = 0;
        $sum = 0;
        foreach ($customers as $key => $value) {
            $sum_of_squares = $sum_of_squares + pow($value->age, 2);
            $sum = $sum + $value->age;
        }

        if ($sum_of_squares == 0) {
            $standard_deviation = 0;
        } else {
            $average_squares = $sum_of_squares / COUNT($customers);

            $sum_ages_customers_squar = $sum / COUNT($customers);
            $sum_ages_customers_squar =  pow($sum_ages_customers_squar, 2);

            $standard_deviation = sqrt($average_squares - $sum_ages_customers_squar);

            $standard_deviation = number_format($standard_deviation, 2);
        }



        $data_card = array(
            'average' => (string) number_format($average, 2),
            'standard_deviation' => (string) $standard_deviation,
            'total_customers' => (string) COUNT($customers),
            'max_age' => (string) $max_age
        );

        return $data_card;
    }

    public function getGraphPie1($start_date, $end_date)
    {
        $labels_1 = array();
        $series_1 = array();

        $customers = Customer::whereBetween('created_at', [$start_date, $end_date])->get();

        $gender_array = array(
            array('id' => '0', 'name' => 'Masculino'),
            array('id' => '1', 'name' => 'Femenino'),
            array('id' => '2', 'name' => 'No definido')
        );

        if (COUNT($customers) > 0) {
            foreach ($gender_array as $key => $value) {
                $sum = 0;
                foreach ($customers as $keycustomer => $valuecustomer) {
                    if ($value['id'] == $valuecustomer->gender) {
                        $sum = $sum + 1;
                    }
                }
                array_push($series_1, $sum);
                array_push($labels_1, $value['name']);
            }
        }

        $data_pies_1 = array(
            'series' => $series_1,
            'labels' => $labels_1
        );

        return $data_pies_1;
    }

    public function getGraphPie2($start_date, $end_date)
    {
        $labels_2 = array();
        $series_2 = array();

        $customers = Customer::whereBetween('created_at', [$start_date, $end_date])->get();

        $ages_array = array();
        foreach ($customers as $key => $value) {
            array_push($ages_array, $value->age);
        }
        $ages_array = array_unique($ages_array); //edades únicas

        $arr = array();
        foreach ($ages_array as $key => $value) {
            $data = [];
            $sum = 0;
            foreach ($customers as $keycustomer => $valuecustomer) {
                if ($value == $valuecustomer->age) {
                    $sum = $sum + 1;
                }
            }
            $data = [
                'age' => $sum,
                'label_age' => $value . ' años',
            ];
            array_push($arr, $data);
        }

        //ordenar el listado de mayor a menor
        rsort($arr);

        foreach ($arr as $key => $value) {
            if ($key < 5) {
                array_push($series_2, $value['age']);
                array_push($labels_2, $value['label_age']);
            }
        }





        $data_pies_2 = array(
            'series' => $series_2,
            'labels' => $labels_2
        );

        return $data_pies_2;
    }

    public function getGraphColumn1()
    {
        //get dates
        $arreglo_dias_atras = array();
        $arreglo_fecha_atras = array();
        array_push($arreglo_dias_atras, Carbon::now()->isoFormat('dddd'));
        array_push($arreglo_fecha_atras, Carbon::now()->toDateString());
        for ($i = 1; $i <= 6; $i++) {
            $fecha_texto = Carbon::now()->subDays($i);
            $dia_texto = $fecha_texto->isoFormat('dddd');
            array_push($arreglo_dias_atras, $dia_texto);
            array_push($arreglo_fecha_atras, $fecha_texto->toDateString());
        }
        $arreglo_dias_atras = array_reverse($arreglo_dias_atras); //fecha de los ultimos 7 días
        $arreglo_fecha_atras = array_reverse($arreglo_fecha_atras);
        //fin get dates

        $customers = Customer::whereBetween('created_at', [Carbon::now()->subDays(7), Carbon::now()])->get();

        $gender_array = array(
            array('id' => '0', 'name' => 'Masculino'),
            array('id' => '1', 'name' => 'Femenino'),
            array('id' => '2', 'name' => 'No definido')
        );

        $array_series = array();
        foreach ($gender_array as $key => $value) {
            $data_last_seven_days = array();
            foreach ($arreglo_fecha_atras as $keyDate => $valueDate) {
                $sum_data = 0;
                foreach ($customers as $keycustomer => $valuecustomer) {
                    if ($valueDate == $valuecustomer->created_at->toDateString() && $value['id'] == $valuecustomer->gender) {
                        $sum_data = $sum_data + 1;
                    }
                }
                array_push($data_last_seven_days, $sum_data);
            }

            $data = [
                'name' => $value['name'],
                'data' => $data_last_seven_days
            ];
            array_push($array_series, $data);
        }
        $data_retornar = [
            'array_dates_back' => $arreglo_fecha_atras,
            'array_days_back' => $arreglo_dias_atras,
            'series' => $array_series
        ];
        return $data_retornar;
    }
}
