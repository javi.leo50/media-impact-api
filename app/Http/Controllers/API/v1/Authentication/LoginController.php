<?php

namespace App\Http\Controllers\API\v1\Authentication;

use App\Models\User;
use App\Http\Requests\API\v1\Authentication\LoginRequest;

use App\DTOs\API\v1\Authentication\LoginDto;
use App\UseCases\API\v1\Authentication\GenerateTokenUser;
use App\UseCases\API\v1\Authentication\VerifyUser;

use App\Http\Controllers\API\v1\ApiController;

class LoginController extends ApiController
{
    public function login(LoginRequest $request)
    {
        $loginDto = LoginDto::fromRequest($request);
        $user = new VerifyUser();
        $user = $user->execute($loginDto);
        if (!$user) {
            return $this->errorResponse(trans('auth.failed'), 401);
        }

        $generate_token = new GenerateTokenUser(User::find($user->id));
        $token = $generate_token->execute($loginDto);

        return $this->showData(['access_token' => $token['tokenUser']->accessToken, 'user' => ['full_name' => $token['user']['name'] . " " . $token['user']['last_name']]], 200);
    }

    public function logout()
    {
        auth()->user()->tokens->each(function ($token, $key) { #Todas las sesiones anteriores
            $token->revoke();
        });
        return $this->showData(trans('custom.message.logout.success'));
    }
}
