<?php

namespace App\Http\Requests\API\v1\Customer;

use Illuminate\Foundation\Http\FormRequest;

class CustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'email' => 'nullable|email',
            'name' => 'required',
            'last_name' => 'required',
            'gender' => 'nullable|digits_between:0,2',
            'date_birthday' => 'required|date_format:d-m-Y',
        ];

        return $rules;
    }
}
