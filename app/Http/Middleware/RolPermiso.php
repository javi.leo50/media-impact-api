<?php

namespace App\Http\Middleware;

use App\Models\Permission;

use Closure;

class RolPermiso
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $link)
    {
        $verificacion = false;
         
        $response = Permission::where('rol_id', auth()->user()->rolRel['id'])->whereHas('module', function ($query) use ($link) {
            $query->where('slug', $link);
        })->first();
        
        if ($response) {
            $verificacion = true;
        } 
         
        if (!$verificacion) {
            return response()->json(
                ["message" => trans('custom.message.auth.forbidden')],
                403
            );
        }
        return $next($request);
    }
}
