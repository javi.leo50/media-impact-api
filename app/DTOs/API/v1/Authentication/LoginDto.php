<?php

namespace App\DTOs\API\v1\Authentication;

use App\Abstracts\DataTransferObject;
use App\Http\Requests\API\v1\Authentication\LoginRequest;

class LoginDto extends DataTransferObject
{
    public $email;
    public $password;
    public $remember_me;

    public static function fromRequest(LoginRequest $request)
    {
        return new self($request->all());
    }
}
