<?php

namespace App\DTOs\API\v1\Customer;

use App\Abstracts\DataTransferObject;
use App\Http\Requests\API\v1\Customer\CustomerRequest;

class UpdateCustomerDto extends DataTransferObject
{
    public $name;
    public $last_name;
    public $email;
    public $gender;
    public $file_image;
    public $date_birthday;

    public static function fromRequest(CustomerRequest $request)
    {
        return new self($request->all());
    }
}
