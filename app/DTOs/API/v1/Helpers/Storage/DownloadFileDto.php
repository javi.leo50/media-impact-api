<?php

namespace App\DTOs\API\v1\Helpers\Storage;

use App\Abstracts\DataTransferObject;

class DownloadFileDto extends DataTransferObject
{
    public $folder;
    public $subfolder;
    public $file;

    public static function fromArray($array)
    {
        return new self($array);
    }
}
