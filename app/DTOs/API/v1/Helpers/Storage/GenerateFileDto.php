<?php

namespace App\DTOs\API\v1\Helpers\Storage;

use App\Abstracts\DataTransferObject;

class GenerateFileDto extends DataTransferObject
{
    public $file;

    public static function fromArray($array)
    {
        return new self($array);
    }
}
