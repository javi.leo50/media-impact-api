<?php

use App\Http\Controllers\API\v1\Authentication\LoginController;
use App\Http\Controllers\API\v1\Customer\CustomerController;
use App\Http\Controllers\API\v1\Dashboard\DashboardController;
use App\Http\Controllers\API\v1\User\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

#Login
Route::post('login', [LoginController::class, 'login']);

Route::prefix('auth')->middleware(['auth:api'])->group(function () {
    #Versión 1
    Route::prefix('v1')->group(function () {
        Route::prefix('dashboard')->group(function () {
            Route::get('/', [DashboardController::class, 'index']);
            Route::post('get-data-filter-cards', [DashboardController::class, 'getDataFilterCards']);
            Route::post('get-data-filter-pies', [DashboardController::class, 'getDataFilterPies']);
        });
        #Dashboard

        #User
        Route::get('user', [UserController::class, 'user']);

        #customers
        Route::apiResource('customer', CustomerController::class);

        #Logout
        Route::post('logout', [LoginController::class, 'logout']);
    });
});
