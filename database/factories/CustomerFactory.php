<?php

namespace Database\Factories;

use App\Models\Customer;
use Illuminate\Database\Eloquent\Factories\Factory;

class CustomerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Customer::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'last_name' => $this->faker->lastName,
            'date_birthday' => $this->faker->date('Y-m-d', '2002-01-01'),
            'gender' => $this->faker->numberBetween(0, 2),
            'email' => $this->faker->unique()->safeEmail,
        ];
    }
}
