<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = 'Javier Leonardo Effio';
        $user->email = 'javier@admin.pe';
        $user->password = Hash::make('12345678');
        $user->rol_id = '1';
        $user->save();

        $user = new User();
        $user->name = 'Media Impact';
        $user->email = 'mediaimpact@admin.pe';
        $user->password = Hash::make('12345678');
        $user->rol_id = '1';
        $user->save();
    }
}
