<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MasterModulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $position = 1;
        DB::table('master_modules')->insert([
            [
                'name'     => 'Dashboard',
                'slug'         => '/dashboard',
                'nameref'   => 'dashboard',
                'parent'     => null,
                'icon'         => 'mdi-home',
                'index'    => $position++
            ],
            [
                'name'     => 'Clientes',
                'slug'         => '/clientes',
                'nameref'   => 'clientes',
                'parent'     => null,
                'icon'         => 'mdi-account-heart',
                'index'    => $position++
            ]
        ]);
    }
}
