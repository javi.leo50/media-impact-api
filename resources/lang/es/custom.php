<?php

return [
    'attribute' => [
        'customer' => 'El cliente',
    ],
    'title' => [
        'error' => 'Error',
        'warning' => 'Advertencia',
        'success' => 'Éxito'
    ],
    'errors' => [
        'image' => 'Ocurrió un error al momento de subir la imagen. Por favor, inténtelo nuevamente.',
        'file' => 'Ocurrió un error al momento de subir el archivo. Por favor, inténtelo nuevamente.',
        'video' => 'Ocurrió un error al momento de subir el video. Por favor, inténtelo nuevamente.'
    ],
    'message' => [
        'auth' => [
            'forbidden' => 'No tiene permisos suficientes.'
        ],
        'disable' => [
            'success' => ':name se ha deshabilitado.',
            'error' => "Lo sentimos. :name no se pudo deshabilitar debido a un error.",
        ],
        'create' => [
            'success' => ':name se ha creado.',
            'error' => "Lo sentimos. :name no se pudo crear debido a un error.",
        ],
        'update' => [
            'success' => ':name se ha actualizado.',
            'error' => "Lo sentimos. :name no se pudo actualizar debido a un error.",
            'plural' => [
                'success' => ':name se han actualizado.',
                'error' => "Lo sentimos. :name no se pudieron actualizar debido a un error.",
            ]
        ],
        'delete' => [
            'success' => ':name se ha eliminado.',
            'error' => "Lo sentimos, :name no se pudo eliminar debido a un error.",
        ],
        'logout' => [
            'success' => 'Sesión cerrada correctamente.',
            'error' => "No se puedo cerrar sesión debido a un error.",
        ],

        'export' => [
            'success' => 'El archivo se ha exportado.',
            'no_data' => [
                'range' => 'No se encontraron resultados en la fecha especificada',
                'total' => 'No se encontraron resultados'
            ]
        ],
        'order'  => 'Arrastre los elementos en el orden que desee mostrarlos',
        'cant_delete_user'  => 'No se puede eliminar debido a que no es un :name',
        'cant_delete_post'  => 'No se puede eliminar debido a que está anidado en al menos un Post'
    ]
];
